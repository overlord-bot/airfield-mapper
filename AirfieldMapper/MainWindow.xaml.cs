﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Microsoft.Msagl.Core.Geometry.Curves;
using Microsoft.Msagl.Core.Layout;
using Microsoft.Msagl.Drawing;
using Microsoft.Msagl.Layout.MDS;
using Microsoft.Msagl.Miscellaneous;
using Microsoft.Msagl.WpfGraphControl;
using Microsoft.Win32;
using QuikGraph;
using RurouniJones.DCS.AirfieldMapper.ChildWindows;
using YamlDotNet.Serialization;
using static System.Int32;
using Label = Microsoft.Msagl.Drawing.Label;

namespace RurouniJones.DCS.AirfieldMapper
{
    public partial class MainWindow : Window
    {
        private static readonly IDeserializer Deserializer = new DeserializerBuilder().Build();
        private static readonly ISerializer Serializer = new SerializerBuilder().Build();

        private string _fileName;
        private static Airfield _airfield;

        private static VNode _sourceNode;
        private static VNode _targetNode;

        private static GraphViewer _graphViewer;
        private static Graph _graph;

        readonly LayoutAlgorithmSettings _layoutSettings = new MdsLayoutSettings();

        internal Airfield.NavigationPoint HighlightedPoint = null;

        private static MarkerImportWindow _markerImport;

        private static NodeEditorWindow _nodeEditor;

        private static AirfieldInformationWindow _airfieldInfo;

        private static EdgeEditorWindow _edgeEditor;

        //Phonetic alphabet, alternate spellings, and double-letter taxiways. Will add Letter+Number taxiways in the event that they show up in numbers.
        //Doubles first so they take priority.
        private const string LikelyTaxiwayString = "Alpha Alpha|Alfa Alfa|Beta Beta|Charlie Charlie|Delta Delta|Golf Golf|Echo Echo|Foxtrot Foxtrot|Hotel Hotel|Juliett Juliett|Juliet Juliet|Kilo Kilo|Lima Lima|Mike Mike|November November|Oscar Oscar|Papa Papa|Quebec Quebec|Sierra Sierra|Tango Tango|Uniform Uniform|Victor Victor|Whiskey Whiskey|Yankee Yankee|Zulu Zulu|Alpha|Alfa|Beta|Charlie|Delta|Golf|Echo|Foxtrot|Hotel|Juliett|Juliet|Kilo|Lima|Mike|November|Oscar|Papa|Quebec|Sierra|Tango|Uniform|Victor|Whiskey|Yankee|Zulu";
        private static IEnumerable<string> LikelyTaxiwayNames => LikelyTaxiwayString.Split('|');

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Load_Airfield(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "YAML files (*.yaml)|*.yaml",
                InitialDirectory = AppDomain.CurrentDomain.BaseDirectory
            };
            if (openFileDialog.ShowDialog() != true) return;
            try
            {
                _fileName = openFileDialog.FileName;

                _airfield = Deserializer.Deserialize<Airfield>(File.ReadAllText(_fileName));
                _airfield.BuildTaxiGraph();
                ReloadAirfieldButton.IsEnabled = true;
                SaveAirfieldButton.IsEnabled = true;
                AddTaxiPathButton.IsEnabled = true;
                DisplayRealGraphButton.IsEnabled = true;
                ShowMarkerImport.IsEnabled = true;
                NewButton.IsEnabled = false;
                AirfieldInfoButton.IsEnabled = true;
                ShowNodeList.IsEnabled = true;
                ShowEdgeList.IsEnabled = true;

                DisplayGraph();
            }
            catch (Exception)
            {
                MessageBox.Show("Error reading Airfield data", "Load Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Reload_Airfield(object sender, RoutedEventArgs e)
        {
            try
            {
                _airfield = Deserializer.Deserialize<Airfield>(File.ReadAllText(_fileName));
                _airfield.BuildTaxiGraph();
                DisplayGraph();
            }
            catch (Exception)
            {
                MessageBox.Show("Error reading Airfield data", "Load Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Save_Airfield(object sender, RoutedEventArgs e)
        {
            try
            {
                var json = Serializer.Serialize(_airfield);
                File.WriteAllText(_fileName, json);

                //Also write to temp dir file.
                File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "temp", "markers.json"), json);
            }
            catch (Exception)
            {
                MessageBox.Show("Error writing Airfield data", "Save Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BuildGraph()
        {
            _graph = new Graph();

            foreach (var navigationPoint in _airfield.NavigationGraph.Vertices)
            {

                var node = _graph.AddNode(navigationPoint.Name as string);
                node.UserData = navigationPoint;
                switch (navigationPoint)
                {
                    case Airfield.Runway _:
                        node.Attr.Shape = Shape.DoubleCircle;
                        node.Attr.Color = Color.Green;
                        break;
                    case Airfield.Junction _:
                        node.Attr.Shape = Shape.Hexagon;
                        node.Attr.Color = Color.Blue;
                        break;
                    case Airfield.ParkingSpot _:
                        node.Attr.Shape = Shape.Octagon;
                        node.Attr.Color = Color.Orange;
                        break;
                    case Airfield.WayPoint _:
                        node.Attr.Shape = Shape.Box;
                        node.Attr.Color = Color.Purple;
                        break;
                    case Airfield.NavigationPoint _:
                        node.Attr.Shape = Shape.Circle;
                        if (navigationPoint == HighlightedPoint) node.Attr.Color = Color.Red;
                        else node.Attr.Color = Color.Black;
                        break;
                }
            }

            foreach (var edge in _airfield.NavigationGraph.Edges)
            {
                var displayEdge = _graph.AddEdge(edge.Source.Name, edge.Tag, edge.Target.Name);
                displayEdge.UserData = edge;

                if (edge.Source is Airfield.Runway && edge.Target is Airfield.WayPoint)
                {
                    displayEdge.Attr.Color = Color.Purple;
                }
                else if (edge.Source is Airfield.WayPoint)
                {
                    displayEdge.Attr.Color = Color.Purple;
                }
                else if (_airfield.NavigationCost[edge] >= 999)
                {
                    displayEdge.Attr.Color = Color.Red;
                }
                else if (_airfield.NavigationCost[edge] >= 100)
                {
                    displayEdge.Attr.Color = Color.Orange;
                }
                else
                {
                    displayEdge.Attr.Color = Color.Green;
                }

                if (edge.Source is Airfield.WayPoint || edge.Target is Airfield.WayPoint)
                {
                    displayEdge.Attr.AddStyle(Microsoft.Msagl.Drawing.Style.Dashed);
                }
            }
        }

        internal void DisplayGraph()
        {
            GraphPanel.Children.Clear();

            BuildGraph();

            if (DisplayRealGraphButton.IsChecked != null && (bool)DisplayRealGraphButton.IsChecked)
            {
                DisplayRealGraph();
            }
            else
            {
                DisplayAbstractGraph();
            }
        }

        private void DisplayAbstractGraph()
        {
            _graphViewer = new GraphViewer
            {
                LayoutEditingEnabled = false,
            };

            _graphViewer.BindToPanel(GraphPanel);
            _graphViewer.MouseDown += MouseDownHandler;
            _graphViewer.MouseUp += MouseUpHandler;
            _graphViewer.Graph = _graph;
        }

        private void DisplayRealGraph()
        {
            _graph.CreateGeometryGraph();

            foreach (var navigationPoint in _airfield.NavigationGraph.Vertices)
            {

                var dnode = _graph.Nodes.FirstOrDefault(node => node.Id.Equals(navigationPoint.Name));
                if (dnode != null)
                {
                    dnode.GeometryNode.BoundaryCurve = CreateLabelAndBoundary(navigationPoint, dnode);
                }
                else
                {
                    MessageBox.Show($"Error Displaying {navigationPoint.Name}", "Display Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            LayoutHelpers.RouteAndLabelEdges(_graph.GeometryGraph, _layoutSettings, _graph.GeometryGraph.Edges);

            _graphViewer = new GraphViewer
            {
                LayoutEditingEnabled = false,
                NeedToCalculateLayout = false
            };

            _graphViewer.BindToPanel(GraphPanel);
            _graphViewer.MouseDown += MouseDownHandler;
            _graphViewer.MouseUp += MouseUpHandler;
            _graphViewer.Graph = _graph;
        }

        private ICurve CreateLabelAndBoundary(Airfield.NavigationPoint navigationPoint, Microsoft.Msagl.Drawing.Node node)
        {
            node.Attr.LabelMargin *= 2;
            node.Label.IsVisible = false;

            var y = (navigationPoint.Latitude - _airfield.Latitude) * 200000;
            var x = (navigationPoint.Longitude - _airfield.Longitude) * 200000;
            var positionalPoint = new Microsoft.Msagl.Core.Geometry.Point(x, y);

            switch (navigationPoint)
            {
                case Airfield.Runway _:
                    node.Attr.Color = Color.Green;
                    return CurveFactory.CreateCircle(50, positionalPoint);
                case Airfield.Junction _:
                    node.Attr.Shape = Shape.Hexagon;
                    node.Attr.Color = Color.Blue;
                    return CurveFactory.CreateHexagon(100, 30, positionalPoint);
                case Airfield.ParkingSpot _:
                    node.Attr.Color = Color.Orange;
                    return CurveFactory.CreateOctagon(100, 30, positionalPoint);
                case Airfield.WayPoint _:
                    node.Attr.Color = Color.Purple;
                    return CurveFactory.CreateRectangle(100, 30, positionalPoint);
                case Airfield.NavigationPoint _:
                    if (navigationPoint == HighlightedPoint) node.Attr.Color = Color.Red;
                    else node.Attr.Color = Color.Black;
                    return CurveFactory.CreateCircle(100, positionalPoint);
            }

            return CurveFactory.CreateCircle(5, positionalPoint);
        }

        private void MouseDownHandler(object s, MsaglMouseEventArgs ev)
        {
            if (!ev.RightButtonIsPressed || !(bool)AddTaxiPathButton.IsChecked)
                return;

            if (_graphViewer.ObjectUnderMouseCursor is VNode node)
            {
                _sourceNode = node;
            }
            else if (_graphViewer.ObjectUnderMouseCursor?.DrawingObject is Label label)
            {
                var taggedEdge = (TaggedEdge<Airfield.NavigationPoint, string>)label.Owner.UserData;

                if (taggedEdge == null)
                    return;

                var taxiPath = _airfield.NavigationPaths.Find(x =>
                    x.Source == taggedEdge.Source.Name && x.Target == taggedEdge.Target.Name);

                var cost = _airfield.NavigationCost[taggedEdge];

                switch (cost)
                {

                    // 0 shouldn't happen but has happened in the past due to bugs so cater to it.
                    case 0:
                        _airfield.NavigationCost[taggedEdge] = 100;
                        taxiPath.Cost = 100;
                        break;
                    case 1:
                        _airfield.NavigationCost[taggedEdge] = 100;
                        taxiPath.Cost = 100;
                        break;
                    case 100:
                        _airfield.NavigationCost[taggedEdge] = 999;
                        taxiPath.Cost = 999;
                        break;
                    case 999:
                        _airfield.NavigationCost[taggedEdge] = 1;
                        taxiPath.Cost = 1;
                        break;
                }

                DisplayGraph();
            }
        }

        private void MouseUpHandler(object s, MsaglMouseEventArgs ev)
        {
            if (_graphViewer.ObjectUnderMouseCursor is VNode node && (bool)AddTaxiPathButton.IsChecked && _sourceNode != null && node != _sourceNode)
            {
                _targetNode = node;

                if (_sourceNode.Node.UserData is Airfield.WayPoint && !(_targetNode.Node.UserData is Airfield.WayPoint) && !(_targetNode.Node.UserData is Airfield.Runway))
                    return;

                if (!(_sourceNode.Node.UserData is Airfield.Runway) && !(_sourceNode.Node.UserData is Airfield.WayPoint) && _targetNode.Node.UserData is Airfield.WayPoint)
                    return;

                var taxiName = _sourceNode.Node.Id.Replace('-', ' ')
                    .Split()
                    .Intersect(_targetNode.Node.Id.Replace('-', ' ').Split())
                    .FirstOrDefault();

                if(taxiName == null || TryParse(taxiName, out _))
                {
                    var tempName = _sourceNode.Node.Id.Replace('-', ' ');

                    foreach(var ln in LikelyTaxiwayNames)
                    {
                        tempName.Contains(ln);
                        taxiName = ln;
                    }
                }

                if (taxiName == null || TryParse(taxiName, out _))
                {
                    var tempName = _targetNode.Node.Id.Replace('-', ' ');
                    foreach (var ln in LikelyTaxiwayNames)
                    {
                        tempName.Contains(ln);
                        taxiName = ln;
                    }
                }

                if (taxiName == null || TryParse(taxiName, out _))
                {
                    taxiName = $"{_sourceNode.Node.Id} to {_targetNode.Node.Id}";
                }

                var mainCost = 1;
                var reverseCost = 1;

                if (_sourceNode.Node.Id.Contains("Apron") || _sourceNode.Node.Id.Contains("Ramp"))
                {
                    reverseCost = 100;
                }
                if (_targetNode.Node.Id.Contains("Apron") || _targetNode.Node.Id.Contains("Ramp"))
                {
                    mainCost = 100;
                }
                if (_sourceNode.Node.Id.Contains("Spot") || _sourceNode.Node.Id.Contains("Maintenance") || _sourceNode.Node.Id.Contains("Bunker") || _sourceNode.Node.Id.Contains("Shelter") || _sourceNode.Node.Id.Contains("Parking") || _sourceNode.Node.Id.Contains("Cargo") || _sourceNode.Node.Id.Contains("Revetment"))
                {
                    reverseCost = 999;
                }
                if (_targetNode.Node.Id.Contains("Spot") || _targetNode.Node.Id.Contains("Maintenance") || _targetNode.Node.Id.Contains("Bunker") || _targetNode.Node.Id.Contains("Shelter") || _targetNode.Node.Id.Contains("Parking") || _targetNode.Node.Id.Contains("Cargo") || _targetNode.Node.Id.Contains("Revetment"))
                {
                    mainCost = 999;
                }
                if (_sourceNode.Node.Id.Contains("Runway") && _targetNode.Node.Id.Contains("Runway"))
                {
                    mainCost = 999;
                    reverseCost = 999;
                }

                // Add to the Taxiways for searching
                _airfield.NavigationPaths.Add(new Airfield.NavigationPath
                {
                    Source = _sourceNode.Node.Id,
                    Target = _targetNode.Node.Id,
                    Name = taxiName,
                    Cost = mainCost,
                });

                _airfield.NavigationPaths.Add(new Airfield.NavigationPath
                {
                    Source = _targetNode.Node.Id,
                    Target = _sourceNode.Node.Id,
                    Name = taxiName,
                    Cost = reverseCost,
                });

                // Add to the underlying graph for data for refreshing UI
                var mainEdge = new TaggedEdge<Airfield.NavigationPoint, string>((Airfield.NavigationPoint)_sourceNode.Node.UserData,
                    (Airfield.NavigationPoint)_targetNode.Node.UserData, taxiName);
                _airfield.NavigationGraph.AddEdge(mainEdge);
                if (_airfield.NavigationCost == null) _airfield.NavigationCost = new Dictionary<TaggedEdge<Airfield.NavigationPoint, string>, double>();
                _airfield.NavigationCost[mainEdge] = mainCost;

                var reverseEdge = new TaggedEdge<Airfield.NavigationPoint, string>((Airfield.NavigationPoint)_targetNode.Node.UserData,
                    (Airfield.NavigationPoint)_sourceNode.Node.UserData, taxiName);
                _airfield.NavigationGraph.AddEdge(reverseEdge);
                _airfield.NavigationCost[reverseEdge] = reverseCost;

                _sourceNode = null;
                DisplayGraph();
            }
        }

        private void DisplayRealGraphButton_Clicked(object sender, RoutedEventArgs e)
        {
            DisplayGraph();
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog()
            {
                Filter = "YAML files (*.yaml)|*.yaml",
                InitialDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Data\Airfields")
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    _fileName = saveFileDialog.FileName;
                    _airfield = new Airfield();
                    Save_Airfield(sender, e);
                }
                catch
                {
                    MessageBox.Show("Failure creating new airfield");
                    return;
                }

                DisplayGraph();

                ReloadAirfieldButton.IsEnabled = true;
                SaveAirfieldButton.IsEnabled = true;
                AddTaxiPathButton.IsEnabled = true;
                DisplayRealGraphButton.IsEnabled = true;
                ShowMarkerImport.IsEnabled = true;
                NewButton.IsEnabled = false;
                AirfieldInfoButton.IsEnabled = true;
                ShowNodeList.IsEnabled = true;
                ShowEdgeList.IsEnabled = true;
            }
        }

        private void ShowMarkerImport_Checked(object sender, RoutedEventArgs e)
        {
            _markerImport ??= new MarkerImportWindow(_airfield, this);
            _markerImport.Closed += MarkerImport_Closed;
            _markerImport.Show();
        }

        private void MarkerImport_Closed(object sender, EventArgs e)
        {
            _markerImport = null;
        }

        private void ShowNodeList_Checked(object sender, RoutedEventArgs e)
        {
            if (_nodeEditor == null) _nodeEditor = new NodeEditorWindow(this, _airfield);
            _nodeEditor.Closed += NodeList_Closed;
            _nodeEditor.Show();
        }

        private void NodeList_Closed(object sender, EventArgs e)
        {
            _nodeEditor = null;
        }

        private void AirfieldInfoButton_Checked(object sender, RoutedEventArgs e)
        {
            _airfieldInfo ??= new AirfieldInformationWindow(_airfield);
            _airfieldInfo.Closed += Airfieldinfo_Closed;
            _airfieldInfo.Show();
        }

        private void Airfieldinfo_Closed(object sender, EventArgs e)
        {
            _airfieldInfo = null;
        }

        private void ShowEdgeList_Click(object sender, RoutedEventArgs e)
        {
            _edgeEditor ??= new EdgeEditorWindow(this, _airfield);
            _edgeEditor.Closed += EdgeEditorClosed;
            _edgeEditor.Show();
        }

        private void EdgeEditorClosed(object sender, EventArgs e)
        {
            _edgeEditor = null;
        }
    }

}
