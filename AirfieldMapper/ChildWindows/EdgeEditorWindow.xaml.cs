﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace RurouniJones.DCS.AirfieldMapper.ChildWindows
{
    public partial class EdgeEditorWindow : Window
    {
        private readonly MainWindow _parent;
        private readonly Airfield _airfield;

        public EdgeEditorWindow(MainWindow parent, Airfield airfield)
        {
            InitializeComponent();
            _parent = parent;
            _airfield = airfield;

            //Populate NodeList
            foreach(var x in _airfield.NavigationGraph.Vertices)
            {
                NodeList.Items.Add(x);
            }
        }

        private void NodeList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Populate EdgeList
            EdgeList.Items.Clear();
            var item = ((Airfield.NavigationPoint)(e.AddedItems[0])).Name;
            foreach( var edge in _airfield.NavigationPaths.Where(x => x.Source == item || x.Target == item))
            {
                EdgeList.Items.Add(edge);
            }
        }

        private void EdgeList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0) return;
            //Populate Edge Properties
            var item = ((Airfield.NavigationPath)(e.AddedItems[0]));
            EdgeName.Text = item.Name;
            EdgeCost.Text = item.Cost.ToString();
            ToLabel.Content = $"To: {item.Target}";
            FromLabel.Content = $"From: {item.Source}";

        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            //Save edge properties
            if(EdgeCost.Text != "1" && EdgeCost.Text != "100" && EdgeCost.Text != "999")
            {
                MessageBox.Show("Cost may only be 1, 100 or 999");
            }
            
            var edge = (Airfield.NavigationPath)(EdgeList.SelectedItem);
            if (edge == null) return;
            edge.Cost = int.Parse(EdgeCost.Text);
            edge.Name = EdgeName.Text;

            //Refresh EdgeList
            EdgeList.Items.Clear();
            var item = ((Airfield.NavigationPoint)(NodeList.SelectedItem)).Name;
            foreach (var listEdge in _airfield.NavigationPaths.Where(x => x.Source == item || x.Target == item))
            {
                EdgeList.Items.Add(listEdge);
            }

            //Refresh graph
            _parent.DisplayGraph();
        }
    }

}
