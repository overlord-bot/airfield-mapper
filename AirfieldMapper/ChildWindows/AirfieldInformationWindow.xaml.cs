﻿using System.Globalization;
using System.Windows;
using static System.Double;

namespace RurouniJones.DCS.AirfieldMapper.ChildWindows
{
    public partial class AirfieldInformationWindow : Window
    {
        private readonly Airfield _airfield;

        public AirfieldInformationWindow(Airfield airfield)
        {
            InitializeComponent();
            _airfield = airfield;
            NameBox.Text = airfield.Name ?? "";
            LatBox.Text = airfield.Latitude.ToString(CultureInfo.InvariantCulture);
            LongBox.Text = airfield.Latitude.ToString(CultureInfo.InvariantCulture);
            AltBox.Text = airfield.Altitude.ToString(CultureInfo.InvariantCulture);
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            _airfield.Name = NameBox.Text;
            try
            {
                _airfield.Latitude = Parse(LatBox.Text);
                _airfield.Longitude = Parse(LongBox.Text);
                _airfield.Altitude = Parse(AltBox.Text);
            }
            catch
            {
                MessageBox.Show("Failed to interpret one of your inputs to a valid number. Data not saved.");
            }
        }
    }
}
